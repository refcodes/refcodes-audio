# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides audio processing functionality such as generating [sine waves](https://en.wikipedia.org/wiki/Sine_wave) or writing generated samples to [`WAV`](https://web.archive.org/web/20120113025807/http://technology.niagarac.on.ca:80/courses/ctec1631/WavFileFormat.html) files or your PC speakers via I/O streams (as of the [`LineOutSoundSampleWriter`](https://www.javadoc.io/doc/org.refcodes/refcodes-audio/latest/org.refcodes.audio/org/refcodes/audio/LineOutSoundSampleWriter.html) or the [`WavSoundSampleWriter`](https://www.javadoc.io/doc/org.refcodes/refcodes-audio/latest/org.refcodes.audio/org/refcodes/audio/WavSoundSampleWriter.html) types).***

## Getting started ##

> Please refer to the [refcodes-audio: Handle WAV files and line-out with just some I/O streams](https://www.metacodes.pro/refcodes/refcodes-audio) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-audio</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-audio/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.