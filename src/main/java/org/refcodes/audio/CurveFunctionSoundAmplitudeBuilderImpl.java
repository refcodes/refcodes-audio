// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.util.function.Function;

/**
 * The {@link CurveFunctionSoundAmplitudeBuilderImpl} implements the
 * {@link CurveFunctionSoundAmplitudeBuilder} interface.
 */
public class CurveFunctionSoundAmplitudeBuilderImpl implements CurveFunctionSoundAmplitudeBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _index = 0;
	private int _samplingRate = SamplingRate.AUDIO_CD.getSamplesPerSecond();
	private int _xOffset = 0;
	private double _yOffset = 0;
	private double _amplitude = 0;
	private double _frequencyInHz = 440;
	private Function<Double, Double> _trigonometricFunction = CurveFunctionFunction.SINE.getFunction();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrigonometricFunction( Function<Double, Double> aFunction ) {
		_trigonometricFunction = aFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Function<Double, Double> getTrigonometricFunction() {
		return _trigonometricFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIndex( int aIndex ) {
		_index = aIndex;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getIndex() {
		return _index;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFrequency( double aFrequencyInHz ) {
		_frequencyInHz = aFrequencyInHz;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getFrequency() {
		return _frequencyInHz;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAmplitude( double aAmplitude ) {
		_amplitude = aAmplitude;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getAmplitude() {
		return _amplitude;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setXOffset( int aXOffset ) {
		_xOffset = aXOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getXOffset() {
		return _xOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setYOffset( double aYOffset ) {
		_yOffset = aYOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getYOffset() {
		return _yOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSamplingRate( int aSamplingRate ) {
		_samplingRate = aSamplingRate;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _samplingRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MonoAmplitude next() {
		final MonoAmplitude theSample = toMonoSample();
		_index++;
		return theSample;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MonoAmplitude toMonoSample() {
		return CurveFunctionSoundAmplitudeBuilder.asMonoSample( _index, _trigonometricFunction, _frequencyInHz, _amplitude, _xOffset, _yOffset, _samplingRate );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MonoAmplitude toMonoSample( int index ) {
		return CurveFunctionSoundAmplitudeBuilder.asMonoSample( index, _trigonometricFunction, _frequencyInHz, _amplitude, _xOffset, _yOffset, _samplingRate );
	}
}
