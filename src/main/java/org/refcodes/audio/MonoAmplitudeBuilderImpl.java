// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.util.Arrays;

/**
 * A {@link MonoAmplitudeBuilderImpl} represents a single amplitude (one for the
 * mono channel) assigned to an according time positioning.
 */
public class MonoAmplitudeBuilderImpl implements MonoAmplitudeBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private double _timeStamp = -1;
	private int _sampligRate = -1;
	private double[] _sampleData = new double[1];
	private long _index = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according sound
	 * sample's settings
	 * 
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public MonoAmplitudeBuilderImpl( MonoAmplitude aAmplitude ) {
		_index = aAmplitude.getIndex();
		_timeStamp = aAmplitude.getTimeStamp();
		_sampligRate = aAmplitude.getSamplingRate();
		if ( aAmplitude.getSampleData() != null ) {
			_sampleData = Arrays.copyOf( aAmplitude.getSampleData(), aAmplitude.getChannelCount() );
		}
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public MonoAmplitudeBuilderImpl( double aAmplitude ) {
		_sampleData[0] = aAmplitude;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public MonoAmplitudeBuilderImpl( double aTimeMillis, double aAmplitude ) {
		_timeStamp = aTimeMillis;
		_sampleData[0] = aAmplitude;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeBuilderImpl( double aTimeMillis, double aAmplitude, int aSamplingRate ) {
		_timeStamp = aTimeMillis;
		_sampleData[0] = aAmplitude;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeBuilderImpl( double aAmplitude, int aSamplingRate ) {
		_sampleData[0] = aAmplitude;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public MonoAmplitudeBuilderImpl( long aIndex, double aAmplitude ) {
		_index = aIndex;
		_sampleData[0] = aAmplitude;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeBuilderImpl( long aIndex, double aAmplitude, int aSamplingRate ) {
		_index = aIndex;
		_sampleData[0] = aAmplitude;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public MonoAmplitudeBuilderImpl( long aIndex, double aTimeMillis, double aAmplitude ) {
		_index = aIndex;
		_timeStamp = aTimeMillis;
		_sampleData[0] = aAmplitude;
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeBuilderImpl( long aIndex, double aTimeMillis, double aAmplitude, int aSamplingRate ) {
		_index = aIndex;
		_timeStamp = aTimeMillis;
		_sampleData[0] = aAmplitude;
		_sampligRate = aSamplingRate;
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according
	 * time and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeBuilderImpl( long aIndex, int aSamplingRate ) {
		_index = aIndex;
		_sampligRate = aSamplingRate;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMonoData( double aAmplitudeData ) {
		if ( _sampleData == null || _sampleData.length != 1 ) {
			synchronized ( this ) {
				if ( _sampleData == null || _sampleData.length != 1 ) {
					_sampleData = new double[1];
				}
			}
		}
		_sampleData[0] = aAmplitudeData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double[] getSampleData() {
		return _sampleData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTimeStamp() {
		return _timeStamp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _sampligRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getIndex() {
		return _index;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIndex( long aIndex ) {
		_index = aIndex;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void increaseIndex() {
		_index++;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decreaseIndex() {
		_index--;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTimeStamp( double aTimeMillis ) {
		_timeStamp = aTimeMillis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSamplingRate( int aSamplingRate ) {
		_sampligRate = aSamplingRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "MonoAmplitudeBuilderImpl [index=" + getIndex() + ", timeStamp=" + getTimeStamp() + ", sampleData=" + getMonoData() + ", sampligRate=" + getSamplingRate();
	}
}
