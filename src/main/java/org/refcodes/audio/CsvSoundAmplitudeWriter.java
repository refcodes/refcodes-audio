// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * The {@link CsvSoundAmplitudeWriter} provides means to write sound samples to a
 * CSV file.
 */
public class CsvSoundAmplitudeWriter extends AbstractCsvAmplitudeWriter<SoundAmplitude, CsvSoundAmplitudeWriter> implements SoundAmplitudeWriter<CsvSoundAmplitudeWriter> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final SoundAmplitudeBuilder _soundSample = new SoundAmplitudeBuilderImpl( 0, SamplingRate.AUDIO_CD.getSamplesPerSecond() );
	private boolean _hasHeader = false;
	private boolean[] _isSampleDataDirty;
	private boolean _isSamplingRateDirty = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvSoundAmplitudeWriter( File aFile ) throws FileNotFoundException {
		super( aFile );

	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the CSV
	 *        records to.
	 */
	public CsvSoundAmplitudeWriter( OutputStream aOutputStream ) {
		super( aOutputStream );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the CSV
	 *        records to.
	 */
	public CsvSoundAmplitudeWriter( PrintStream aPrintStream ) {
		super( aPrintStream );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvSoundAmplitudeWriter( File aFile, CsvDeltaMode aDeltaMode ) throws FileNotFoundException {
		super( aFile, aDeltaMode );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the CSV
	 *        records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 */
	public CsvSoundAmplitudeWriter( OutputStream aOutputStream, CsvDeltaMode aDeltaMode ) {
		super( aOutputStream, aDeltaMode );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeWriter} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the CSV
	 *        records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 */
	public CsvSoundAmplitudeWriter( PrintStream aPrintStream, CsvDeltaMode aDeltaMode ) {
		super( aPrintStream, aDeltaMode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeNext( double... aAmplitudeData ) {
		if ( aAmplitudeData == null || aAmplitudeData.length == 0 ) {
			throw new IllegalArgumentException( "You must provide at least one sample value, bit you provided " + ( aAmplitudeData == null ? "<null>" : " an empty array" ) + "!" );
		}
		doUpdateSampleData( aAmplitudeData );
		_soundSample.updateTimeStamp();
		writeNext( _soundSample );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeNext( SoundAmplitude aAmplitude ) {
		final double[] theSample = aAmplitude.getSampleData();
		final String[] theRow = new String[theSample.length + 3];

		// Header |-->
		if ( !_hasHeader ) {
			synchronized ( this ) {
				if ( !_hasHeader ) {
					theRow[0] = HEADER_INDEX;
					theRow[1] = HEADER_TIME_STAMP;
					if ( theSample.length > 1 ) {
						for ( int i = 0; i < theSample.length; i++ ) {
							theRow[i + 2] = HEADER_CHANNEL + CHANNEL_INDEX_SEPARATOR + i;
						}
					}
					else {
						theRow[2] = HEADER_SAMPLE_DATA;
					}
					theRow[theRow.length - 1] = HEADER_SAMPLING_RATE;
					_csvWriter.writeHeader( theRow );
					_hasHeader = true;
				}
			}
		}
		// Header <--|

		if ( aAmplitude != _soundSample ) {
			doUpdateSampleData( aAmplitude.getSampleData() );
			if ( aAmplitude.getSamplingRate() != -1 && aAmplitude.getSamplingRate() != _soundSample.getSamplingRate() ) {
				_soundSample.setSamplingRate( aAmplitude.getSamplingRate() );
				_isSamplingRateDirty = true;
			}
			if ( aAmplitude.getIndex() != -1 ) {
				_soundSample.setIndex( aAmplitude.getIndex() );
			}
			if ( aAmplitude.getTimeStamp() != -1 ) {
				_soundSample.setTimeStamp( aAmplitude.getTimeStamp() );
			}
		}

		theRow[0] = Long.toString( _soundSample.getIndex() );
		theRow[1] = toString( _soundSample.getTimeStamp() );

		for ( int i = 0; i < theSample.length; i++ ) {
			if ( !_deltaMode.isSampleDataDelta() || _isSampleDataDirty[i] ) {
				theRow[i + 2] = toString( theSample[i] );
			}
		}
		if ( !_deltaMode.isSamplingRateDelta() || _isSamplingRateDirty ) {
			theRow[theRow.length - 1] = Integer.toString( _soundSample.getSamplingRate() );
		}
		_csvWriter.writeNext( theRow );
		_soundSample.increaseIndex();
		Arrays.fill( _isSampleDataDirty, false );
		_isSamplingRateDirty = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _soundSample.getSamplingRate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSamplingRate( int aSamplingRate ) {
		if ( aSamplingRate != -1 && aSamplingRate != _soundSample.getSamplingRate() ) {
			_soundSample.setSamplingRate( aSamplingRate );
			_isSamplingRateDirty = true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvSoundAmplitudeWriter withSamplingRate( int aSamplingRate ) {
		setSamplingRate( aSamplingRate );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Updates the sample data alongside the according dirty flags.
	 * 
	 * @param aAmplitudeData The amplitude data with which to update this instance's
	 *        sample data.
	 */
	private void doUpdateSampleData( double... aAmplitudeData ) {
		if ( _isSampleDataDirty == null ) {
			_isSampleDataDirty = new boolean[aAmplitudeData.length];
			Arrays.fill( _isSampleDataDirty, true );
		}
		final double[] theSampleData = _soundSample.getSampleData();
		for ( int i = 0; i < aAmplitudeData.length; i++ ) {
			if ( theSampleData == null || theSampleData[i] != aAmplitudeData[i] ) {
				_isSampleDataDirty[i] = true;
			}
		}
		_soundSample.setSampleData( aAmplitudeData );
	}
}
