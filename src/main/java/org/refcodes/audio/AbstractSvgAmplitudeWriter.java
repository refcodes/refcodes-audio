// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * The {@link AbstractSvgAmplitudeWriter} provides a foundation to write sound
 * samples to a SVG file.
 *
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeWriter} implementation is to operate on.
 * @param <B> The {@link AmplitudeWriter} implementing this
 *        {@link AbstractSvgAmplitudeWriter}.
 */
public abstract class AbstractSvgAmplitudeWriter<S extends SoundAmplitude, B extends AmplitudeWriter<S, B>> implements AmplitudeWriter<S, B> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected PrintStream _printStream;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractSvgAmplitudeWriter} for writing sound
	 * samples to a SVG file or stream.
	 * 
	 * @param aFile The {@link File} where to write the SVG records to.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public AbstractSvgAmplitudeWriter( File aFile ) throws FileNotFoundException {
		this( new PrintStream( aFile ) );

	}

	/**
	 * Constructs the {@link AbstractSvgAmplitudeWriter} for writing sound
	 * samples to a SVG file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the SVG
	 *        records to.
	 */
	public AbstractSvgAmplitudeWriter( OutputStream aOutputStream ) {
		this( new PrintStream( aOutputStream ) );
	}

	/**
	 * Constructs the {@link AbstractSvgAmplitudeWriter} for writing sound
	 * samples to a SVG file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the SVG
	 *        records to.
	 */
	public AbstractSvgAmplitudeWriter( PrintStream aPrintStream ) {
		_printStream = aPrintStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_printStream.println( "</svg>" );
		_printStream.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////
	protected double toYCoordinate( double aAmplitudeData, double aFactor ) {
		return aAmplitudeData * aFactor / 2 + ( aFactor / 2 );
	}

	/**
	 * Writes the SVG file's header, the SVG's tail is written upon invoking
	 * {@link #close()}.
	 */
	protected void writeSvgHeader() {
		_printStream.println( "<svg>" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a double to a string by hackishly removing trailing zeros if
	 * there is no additional value from them unifying the decimal point.
	 * 
	 * @param aDouble The double to be converted to a string.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static String toString( double aDouble ) {
		return format( Double.toString( aDouble ) );
	}

	/**
	 * Converts a string to a double by hackishly removing trailing zeros if
	 * there is no additional value from them and unifying the decimal point.
	 * 
	 * @param aDouble The double from which to remove trailing zeros.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static double toDouble( String aDouble ) {
		return Double.valueOf( format( aDouble ) );
	}

	/**
	 * Hackishly removes trailing zeros if there is no additional value from
	 * them unifying the decimal point.
	 * 
	 * @param aDouble The double from which to remove trailing zeros.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static String format( String aDouble ) {
		int index = aDouble.indexOf( '.' );
		if ( index == -1 ) {
			index = aDouble.indexOf( ',' );
		}
		if ( index != -1 ) {
			while ( aDouble.endsWith( "0" ) ) {
				aDouble = aDouble.substring( 0, aDouble.length() - 1 );
			}
			if ( aDouble.endsWith( "." ) || aDouble.endsWith( "," ) ) {
				aDouble = aDouble.substring( 0, aDouble.length() - 1 );
			}
		}
		return aDouble;
	}
}
