// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import org.refcodes.audio.SamplingRateAccessor.SamplingRateBuilder;
import org.refcodes.audio.SamplingRateAccessor.SamplingRateProperty;
import org.refcodes.mixin.IndexAccessor.IndexBuilder;
import org.refcodes.mixin.IndexAccessor.IndexProperty;
import org.refcodes.mixin.TimeStampAccessor.TimeStampBuilder;
import org.refcodes.mixin.TimeStampAccessor.TimeStampProperty;

/**
 * Base interface for sample builders such as the {@link MonoAmplitudeBuilder}
 * or the {@link SoundAmplitudeBuilder}.
 * 
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeBuilder} implementation is to operate on.
 * @param <B> The {@link AmplitudeBuilder} implementing this
 *        {@link AmplitudeBuilder}.
 */
public interface AmplitudeBuilder<S extends SoundAmplitude, B extends AmplitudeBuilder<S, B>> extends SoundAmplitude, IndexProperty, IndexBuilder<B>, SamplingRateProperty, SamplingRateBuilder<B>, TimeStampProperty, TimeStampBuilder<B> {

	/**
	 * Calculates the time stamp from the sampling rate and the index and sets
	 * the time stamp property (as of {@link #getTimeStamp()}).
	 */
	default void updateTimeStamp() {
		setTimeStamp( ( (double) getIndex() / ( (double) getSamplingRate() ) ) );
	}
}
