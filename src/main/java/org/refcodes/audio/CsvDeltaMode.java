// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * The {@link CsvDeltaMode} defines for which values of a {@link SoundAmplitude}
 * only the delta between the current {@link SoundAmplitude} being written and the
 * previous {@link SoundAmplitude} being written are written to the next row of the
 * CSV file.
 */
public enum CsvDeltaMode {

	/**
	 * Delta for all applicable values: Write only the changes for all values
	 * between the previous {@link SoundAmplitude} and the current
	 * {@link SoundAmplitude} to the next CSV row.
	 */
	ALL(true, true),

	/**
	 * Delta only for the sampling rate: Write only the changes regarding the
	 * previous {@link SoundAmplitude}'s sample rate and the current
	 * {@link SoundAmplitude}'s rate to the next CSV row. Recommended when you are
	 * working with a fixed sampling rate.
	 */
	SAMPLING_RATE(true, false),

	/**
	 * Delta only for the channels' sample data: Write only the changes
	 * regarding the previous {@link SoundAmplitude}'s sample data and the current
	 * {@link SoundAmplitude}'s sample data to the next CSV row.
	 */
	SAMPLE_DATA(false, true),

	/**
	 * Always write the full {@link SoundAmplitude}'s value to the next CSV row.
	 * May produce some redundancy overhead.
	 */
	NONE(false, false);

	private boolean _isSamplingRateDelta;
	private boolean _isSampleDataDelta;

	private CsvDeltaMode( boolean isSamplingRateDelta, boolean isSampleDataDelta ) {
		_isSampleDataDelta = isSampleDataDelta;
		_isSamplingRateDelta = isSamplingRateDelta;
	}

	/**
	 * Determines whether to write only the sampling rate's deltas per row or
	 * explicitly the sampling rate to each row, even when it does not change.
	 * 
	 * @return True when only the deltas of the sampling rate are to be written,
	 *         else false.
	 */
	public boolean isSamplingRateDelta() {
		return _isSamplingRateDelta;
	}

	/**
	 * Determines whether to write only the sampling data's deltas per row or
	 * explicitly the sampling data to each row, even when it does not change.
	 * 
	 * @return True when only the deltas of the sampling data are to be written,
	 *         else false.
	 */
	public boolean isSampleDataDelta() {
		return _isSampleDataDelta;
	}
}
