// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipException;

import org.refcodes.tabular.CsvStringRecordReader;
import org.refcodes.tabular.Record;

/**
 * The {@link CsvSoundAmplitudeReader} provides means to read sound samples from a
 * CSV file.
 */
public class CsvSoundAmplitudeReader extends AbstractCsvAmplitudeReader<SoundAmplitude> implements SoundAmplitudeReader {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final SoundAmplitudeBuilderImpl _soundSample = new SoundAmplitudeBuilderImpl( (long) 0, SamplingRate.AUDIO_CD.getSamplesPerSecond() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link CsvSoundAmplitudeReader} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * 
	 * @throws IOException thrown in case there was an I/O related problem.
	 * @throws ZipException Signals that a Zip exception of some sort has
	 *         occurred.
	 */
	public CsvSoundAmplitudeReader( File aFile ) throws IOException {
		super( aFile );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeReader} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aInputStream The {@link InputStream} where to write the CSV
	 *        records to.
	 *
	 * @throws IOException thrown in case there was an I/O related problem.
	 */
	public CsvSoundAmplitudeReader( InputStream aInputStream ) throws IOException {
		super( aInputStream );
	}

	/**
	 * Constructs the {@link CsvSoundAmplitudeReader} for writing sound samples to
	 * a CSV file or stream.
	 * 
	 * @param aCsvReader The {@link CsvStringRecordReader} with which to write
	 *        the CSV records with.
	 */
	protected CsvSoundAmplitudeReader( CsvStringRecordReader aCsvReader ) {
		super( aCsvReader );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SoundAmplitude nextRow() throws IOException {
		doProbeHeader();

		final Record<String> theSampleCsv = _csvReader.next();

		String eSampleDataCsv;
		final List<Double> theSamplingDatas = new ArrayList<>();

		if ( theSampleCsv.containsKey( CsvSoundAmplitudeWriter.HEADER_SAMPLE_DATA ) ) {
			eSampleDataCsv = theSampleCsv.get( CsvSoundAmplitudeWriter.HEADER_SAMPLE_DATA );
			theSamplingDatas.add( eSampleDataCsv != null ? CsvSoundAmplitudeWriter.toDouble( eSampleDataCsv ) : null );
		}
		else {
			int index = 0;
			String eKey = CsvSoundAmplitudeWriter.HEADER_CHANNEL + CsvSoundAmplitudeWriter.CHANNEL_INDEX_SEPARATOR + index;
			while ( theSampleCsv.containsKey( eKey ) ) {
				eSampleDataCsv = theSampleCsv.get( eKey );
				theSamplingDatas.add( eSampleDataCsv != null ? CsvSoundAmplitudeWriter.toDouble( eSampleDataCsv ) : null );
				index++;
				eKey = CsvSoundAmplitudeWriter.HEADER_CHANNEL + CsvSoundAmplitudeWriter.CHANNEL_INDEX_SEPARATOR + index;
			}
		}

		double[] theSample = _soundSample.getSampleData();
		if ( theSample == null || theSample.length == 0 ) {
			theSample = new double[theSamplingDatas.size()];
			_soundSample.setSampleData( theSample );
		}

		for ( int i = 0; i < theSample.length; i++ ) {
			if ( theSamplingDatas.get( i ) != null ) {
				theSample[i] = theSamplingDatas.get( i );
			}
		}

		final String theIndex = theSampleCsv.get( CsvSoundAmplitudeWriter.HEADER_INDEX );
		if ( theIndex != null && theIndex.length() != 0 ) {
			_soundSample.setIndex( Long.parseLong( theIndex ) );
		}
		else {
			_soundSample.increaseIndex();
		}

		final String theSamplingRate = theSampleCsv.get( CsvSoundAmplitudeWriter.HEADER_SAMPLING_RATE );
		if ( theSamplingRate != null && theSamplingRate.length() != 0 ) {
			_soundSample.setSamplingRate( Integer.parseInt( theSamplingRate ) );
		}

		final String theTimeStamp = theSampleCsv.get( CsvSoundAmplitudeWriter.HEADER_TIME_STAMP );
		if ( theTimeStamp != null && theTimeStamp.length() != 0 ) {
			final BigDecimal theDecimal = new BigDecimal( theTimeStamp );
			_soundSample.setTimeStamp( theDecimal.toBigInteger().doubleValue() );
		}
		else {
			_soundSample.updateTimeStamp();
		}
		return new SoundAmplitudeImpl( _soundSample );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String nextRaw() {
		return _csvReader.nextRaw();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double[] nextSampleData() throws IOException {
		return nextRow().getSampleData();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _soundSample.getSamplingRate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getIndex() {
		return _soundSample.getIndex();
	}
}
