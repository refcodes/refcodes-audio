// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact provides audio processing functionality such as generating
 * <a href="https://en.wikipedia.org/wiki/Sine_wave">sine waves</a> or writing
 * generated samples to <a href=
 * "https://web.archive.org/web/20120113025807/http://technology.niagarac.on.ca:80/courses/ctec1631/WavFileFormat.html">WAV</a>
 * files or your PC speakers via I/O streams (as of the <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-audio/latest/org.refcodes.audio/org/refcodes/audio/LineOutSoundSampleWriter.html">LineOutSoundSampleWriter</a>
 * or the <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-audio/latest/org.refcodes.audio/org/refcodes/audio/WavSoundSampleWriter.html">WavSoundSampleWriter</a>
 * types).
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-audio"><strong>refcodes-audio:
 * Handle WAV files and line-out with just some I/O streams...</strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 */
package org.refcodes.audio;
