// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * The {@link SvgMonoAmplitudeWriter} provides means to write sound samples to a
 * SVG file.
 */
public class SvgMonoAmplitudeWriter extends AbstractSvgAmplitudeWriter<MonoAmplitude, SvgMonoAmplitudeWriter> implements MonoAmplitudeWriter<SvgMonoAmplitudeWriter> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int Y_SCALE_FACTOR = 500;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final MonoAmplitudeBuilder _soundSample = new MonoAmplitudeBuilderImpl( 0, SamplingRate.AUDIO_CD.getSamplesPerSecond() );
	private boolean _hasHeader = false;
	private boolean _isFirst = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link SvgMonoAmplitudeWriter} for writing sound samples to a
	 * SVG file or stream.
	 * 
	 * @param aFile The {@link File} where to write the SVG records to.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public SvgMonoAmplitudeWriter( File aFile ) throws FileNotFoundException {
		super( aFile );

	}

	/**
	 * Constructs the {@link SvgMonoAmplitudeWriter} for writing sound samples to a
	 * SVG file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the SVG
	 *        records to.
	 */
	public SvgMonoAmplitudeWriter( OutputStream aOutputStream ) {
		super( aOutputStream );
	}

	/**
	 * Constructs the {@link SvgMonoAmplitudeWriter} for writing sound samples to a
	 * SVG file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the SVG
	 *        records to.
	 */
	public SvgMonoAmplitudeWriter( PrintStream aPrintStream ) {
		super( aPrintStream );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeNext( double aAmplitudeData ) {
		_soundSample.setMonoData( aAmplitudeData );
		_soundSample.updateTimeStamp();
		writeNext( _soundSample );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeNext( MonoAmplitude aAmplitude ) {

		// Header |-->
		if ( !_hasHeader ) {
			synchronized ( this ) {
				if ( !_hasHeader ) {
					writeSvgHeader();
					_printStream.print( "\t<polyline points=\"" );
					_hasHeader = true;
				}
			}
		}
		// Header <--|

		if ( aAmplitude != _soundSample ) {
			_soundSample.setMonoData( aAmplitude.getMonoData() );
			if ( aAmplitude.getSamplingRate() != -1 && aAmplitude.getSamplingRate() != _soundSample.getSamplingRate() ) {
				_soundSample.setSamplingRate( aAmplitude.getSamplingRate() );
			}
			if ( aAmplitude.getIndex() != -1 ) {
				_soundSample.setIndex( aAmplitude.getIndex() );
			}
			if ( aAmplitude.getTimeStamp() != -1 ) {
				_soundSample.setTimeStamp( aAmplitude.getTimeStamp() );
			}
		}
		if ( !_isFirst ) {
			_printStream.print( " " + toString( _soundSample.getIndex() ) + "," + toString( toYCoordinate( _soundSample.getMonoData(), Y_SCALE_FACTOR ) ) );
		}
		else {
			_printStream.print( toString( _soundSample.getIndex() ) + "," + toString( toYCoordinate( _soundSample.getMonoData(), Y_SCALE_FACTOR ) ) );
			_isFirst = false;

		}
		_soundSample.increaseIndex();
	}

	@Override
	public void close() throws IOException {
		_printStream.println( "\" style=\"fill:none;stroke:black;stroke-width:1\" >" );
		_printStream.println( "\t\t<title>Mono</title>" );
		_printStream.println( "\t</polyline>" );
		super.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _soundSample.getSamplingRate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSamplingRate( int aSamplingRate ) {
		if ( aSamplingRate != -1 && aSamplingRate != _soundSample.getSamplingRate() ) {
			_soundSample.setSamplingRate( aSamplingRate );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SvgMonoAmplitudeWriter withSamplingRate( int aSamplingRate ) {
		setSamplingRate( aSamplingRate );
		return this;
	}
}
