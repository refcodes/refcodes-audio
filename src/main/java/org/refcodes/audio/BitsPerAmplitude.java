// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * The {@link BitsPerAmplitude} enumeration defines some common bits/amplitude per
 * channel constants.
 */
public enum BitsPerAmplitude {

	/**
	 * Low resolution of 8 bits/amplitude and channel.
	 */
	LOW_RES(8),

	/**
	 * High resolution of 16 bits/amplitude and channel.
	 */
	HIGH_RES(16);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _bitsPerSample;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private BitsPerAmplitude( int aResolution ) {
		_bitsPerSample = aResolution;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the bits/amplitude per channel.
	 * 
	 * @return The bits/amplitude for this {@link BitsPerAmplitude}.
	 */
	public int getBitCount() {
		return _bitsPerSample;
	}

	/**
	 * Returns the bytes/sample per channel.
	 * 
	 * @return The bytes/sample for this {@link BitsPerAmplitude}.
	 */
	public int getByteCount() {
		return _bitsPerSample / 8;
	}

	/**
	 * Retrieves the lowest bits/amplitude resolution.
	 * 
	 * @return The lowest resolution.
	 */
	public static BitsPerAmplitude getLowestResolution() {
		return LOW_RES;
	}

	/**
	 * Retrieves the lowest bits/amplitude resolution.
	 * 
	 * @return The highest resolution.
	 */
	public static BitsPerAmplitude getHigestResolution() {
		return HIGH_RES;
	}

	/**
	 * Retrieves the next higher bits/amplitude or null if it is already the
	 * highest bits/amplitude.
	 * 
	 * @return The next higher bits/amplitude or null if already the highest bits /
	 *         sample.
	 */
	public BitsPerAmplitude getNextHigherResolution() {
		return toNextHigherResolution( this );
	}

	/**
	 * Retrieves the previous lower bits/amplitude or null if it is already the
	 * lowest bits/amplitude.
	 * 
	 * @return The previous lower bits/amplitude or null if already the lowest
	 *         bits/amplitude.
	 */
	public BitsPerAmplitude getPreviousLowerResolution() {
		return toPreviousLowerResolution( this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the next higher bits/amplitude or null if it is already the
	 * highest bits/amplitude.
	 * 
	 * @param aResolution The bits/amplitude for which to get the next higher one.
	 * 
	 * @return The next higher bits/amplitude or null if already the highest
	 *         bits/amplitude.
	 */
	private static BitsPerAmplitude toNextHigherResolution( BitsPerAmplitude aResolution ) {
		for ( int i = 0; i < values().length - 1; i++ ) {
			if ( aResolution == values()[i] ) {
				return values()[i + 1];
			}
		}
		return null;
	}

	/**
	 * Retrieves the previous lower bits/amplitude or null if it is already the
	 * lowest bits/amplitude.
	 * 
	 * @param aResolution The bits/amplitude for which to get the previous lower
	 *        one.
	 * 
	 * @return The previous lower bits/amplitude or null if already the lowest
	 *         bits/amplitude.
	 */
	private static BitsPerAmplitude toPreviousLowerResolution( BitsPerAmplitude aResolution ) {
		for ( int i = 1; i < values().length; i++ ) {
			if ( aResolution == values()[i] ) {
				return values()[i - 1];
			}
		}
		return null;
	}
}
