// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * A {@link MonoAmplitudeImpl} represents a single amplitude (one for the mono
 * channel) assigned to an according time positioning.
 */
public class MonoAmplitudeImpl extends SoundAmplitudeImpl implements MonoAmplitude {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according sound
	 * sample's settings
	 * 
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 */
	public MonoAmplitudeImpl( MonoAmplitude aAmplitude ) {
		super( aAmplitude );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 */
	public MonoAmplitudeImpl( double aAmplitude ) {
		super( new double[] { aAmplitude } );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 */
	public MonoAmplitudeImpl( double aTimeMillis, double aAmplitude ) {
		super( aTimeMillis, new double[] { aAmplitude } );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeImpl( double aTimeMillis, double aAmplitude, int aSamplingRate ) {
		super( aTimeMillis, new double[] { aAmplitude }, aSamplingRate );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeImpl( double aAmplitude, int aSamplingRate ) {
		super( new double[] { aAmplitude }, aSamplingRate );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 */
	public MonoAmplitudeImpl( long aIndex, double aAmplitude ) {
		super( aIndex, new double[] { aAmplitude } );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeImpl( long aIndex, double aAmplitude, int aSamplingRate ) {
		super( aIndex, new double[] { aAmplitude }, aSamplingRate );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 */
	public MonoAmplitudeImpl( long aIndex, double aTimeMillis, double aAmplitude ) {
		super( aIndex, aTimeMillis, new double[] { aAmplitude } );
	}

	/**
	 * Constructs a {@link MonoAmplitude} instance with the according time and
	 * sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitude The mono sample (a single value representing the mono
	 *        channel) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public MonoAmplitudeImpl( long aIndex, double aTimeMillis, double aAmplitude, int aSamplingRate ) {
		super( aIndex, aTimeMillis, new double[] { aAmplitude }, aSamplingRate );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "MonoAmplitudeImpl [index=" + getIndex() + ", timeStamp=" + getTimeStamp() + ", sampleData=" + getMonoData() + ", sampligRate=" + getSamplingRate() + "]";
	}
}
