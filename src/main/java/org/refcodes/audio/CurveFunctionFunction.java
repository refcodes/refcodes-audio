// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.util.function.Function;

/**
 * Enumeration containing some predefined trigonometric functions.
 */
public enum CurveFunctionFunction {

	SINE(Math::sin),

	COSINE(Math::cos),

	TANGENT(Math::tan),

	SAWTOOTH(x -> x - ( Math.floor( x ) * 2 ) - 1),

	SQUARE(x -> Math.sin( x ) > 0 ? 1 : -1.0),

	TRIANGLE(x -> Math.abs( ( x++ % 4 ) - 2 ));

	private Function<Double, Double> _funtion;

	private CurveFunctionFunction( Function<Double, Double> aFunction ) {
		_funtion = aFunction;
	}

	/**
	 * Returns the according trigonometric function.
	 *
	 * @return The according according trigonometric function.
	 */
	public Function<Double, Double> getFunction() {
		return _funtion;
	}
}
