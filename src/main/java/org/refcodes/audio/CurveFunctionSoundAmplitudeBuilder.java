// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.util.function.Function;

import org.refcodes.audio.SamplingRateAccessor.SamplingRateBuilder;
import org.refcodes.audio.SamplingRateAccessor.SamplingRateProperty;

/**
 * The {@link CurveFunctionSoundAmplitudeBuilder} makes it easy to create sound
 * samples for some (trigonometric) functions. The provided function should
 * return double values between -1 and 1.
 */
public interface CurveFunctionSoundAmplitudeBuilder extends SamplingRateProperty, SamplingRateBuilder<CurveFunctionSoundAmplitudeBuilder> {

	/**
	 * Sets the trigonometric function to be used when generating
	 * {@link MonoAmplitude}.
	 * 
	 * @param aFunction The {@link Function} to be set.
	 */
	void setTrigonometricFunction( Function<Double, Double> aFunction );

	/**
	 * Returns the trigonometric function being used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @return The {@link Function} being set.
	 */
	Function<Double, Double> getTrigonometricFunction();

	/**
	 * Builder method for setting the trigonometric function to be used when
	 * generating {@link MonoAmplitude} instances.
	 * 
	 * @param aFunction The {@link Function} to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withTrigonometricFunction( Function<Double, Double> aFunction ) {
		setTrigonometricFunction( aFunction );
		return this;
	}

	/**
	 * Sets the trigonometric function to be used when generating
	 * {@link MonoAmplitude}.
	 * 
	 * @param aFunction The {@link CurveFunctionFunction} to be set.
	 */
	default void setTrigonometricFunction( CurveFunctionFunction aFunction ) {
		setTrigonometricFunction( aFunction.getFunction() );
	}

	/**
	 * Builder method for setting the trigonometric function to be used when
	 * generating {@link MonoAmplitude} instances.
	 * 
	 * @param aFunction The {@link CurveFunctionFunction} to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withTrigonometricFunction( CurveFunctionFunction aFunction ) {
		setTrigonometricFunction( aFunction.getFunction() );
		return this;
	}

	/**
	 * Sets the index to be used when generating {@link MonoAmplitude} instances.
	 * 
	 * @param aIndex The index to be set.
	 */
	void setIndex( int aIndex );

	/**
	 * Returns the index being used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @return The index being set.
	 */
	int getIndex();

	/**
	 * Builder method for setting the index to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aIndex The index to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withIndex( int aIndex ) {
		setIndex( aIndex );
		return this;
	}

	/**
	 * Sets the frequency in Hz to be used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @param aFrequencyInHz The frequency to be set.
	 */
	void setFrequency( double aFrequencyInHz );

	/**
	 * Returns the frequency in Hz being used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @return The frequency in Hz being set.
	 */
	double getFrequency();

	/**
	 * Builder method for setting the frequency in Hz to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aFrequencyInHz The frequency in Hz to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withFrequency( double aFrequencyInHz ) {
		setFrequency( aFrequencyInHz );
		return this;
	}

	/**
	 * Sets the amplitude to be used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @param aAmplitude The amplitude to be set.
	 */
	void setAmplitude( double aAmplitude );

	/**
	 * Returns the amplitude being used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @return The amplitude being set.
	 */
	double getAmplitude();

	/**
	 * Builder method for setting the amplitude to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aAmplitude The amplitude to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withAmplitude( double aAmplitude ) {
		setAmplitude( aAmplitude );
		return this;
	}

	/**
	 * Sets the x-offset to be used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @param aXOffset The x-offset to be set.
	 */
	void setXOffset( int aXOffset );

	/**
	 * Returns the x-offset being used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @return The x-offset being set.
	 */
	int getXOffset();

	/**
	 * Builder method for setting the x-offset to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aXOffset The x-offset to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withXOffset( int aXOffset ) {
		setXOffset( aXOffset );
		return this;
	}

	/**
	 * Sets the y-offset to be used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @param aYOffset The y-offset to be set.
	 */
	void setYOffset( double aYOffset );

	/**
	 * Returns the yOffset being used when generating {@link MonoAmplitude}
	 * instances.
	 * 
	 * @return The yOffset being set.
	 */
	double getYOffset();

	/**
	 * Builder method for setting the y-offset to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aYOffset The y-offset to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	default CurveFunctionSoundAmplitudeBuilder withYOffset( double aYOffset ) {
		setYOffset( aYOffset );
		return this;
	}

	/**
	 * Builder method for setting the sampling rate to be used when generating
	 * {@link MonoAmplitude} instances.
	 * 
	 * @param aSamplingRate The sampling rate to be set.
	 * 
	 * @return This {@link CurveFunctionSoundAmplitudeBuilder} instance for
	 *         chaining multiple builder operations.
	 */
	@Override
	default CurveFunctionSoundAmplitudeBuilder withSamplingRate( int aSamplingRate ) {
		setSamplingRate( aSamplingRate );
		return null;
	}

	/**
	 * Returns the next {@link MonoAmplitude} for the current index (as of
	 * {@link #getIndex()}) as well as for the other builder's settings and
	 * increases the index by one.
	 * 
	 * @return The next {@link MonoAmplitude} being set up.
	 */
	MonoAmplitude next();

	/**
	 * Creates a {@link MonoAmplitude} as of the builder's settings.
	 * 
	 * @return The sound sample as of the builder's settings.
	 */
	MonoAmplitude toMonoSample();

	/**
	 * Creates a {@link MonoAmplitude} for the given index and the the builder's
	 * settings.
	 *
	 * @param aIndex The index for which to create the sound sample.
	 * 
	 * @return The sound sample as of the index and the builder's settings.
	 */
	MonoAmplitude toMonoSample( int aIndex );

	/**
	 * Creates a {@link MonoAmplitude} for the given arguments and the given
	 * function. Thanks to willemsenzo, May 18, 2017:
	 * <code>return Mathf.Sin(2 * Mathf.PI * timeIndex * frequency / sampleRate);</code>
	 * See "https://forum.unity.com/threads/generating-a-simple-sinewave.471529"
	 * 
	 * @param aIndex The index of the sample to generate.
	 * @param aFunction The function to be applied (sine, cosine, ...).
	 * @param aFrequencyHz The frequency (Hz) to use.
	 * @param aAmplitude The maximum amplitude to produce.
	 * @param aXOffset The x-offset (in samples) to be used.
	 * @param aYOffset The y-offset for the amplitude to be used.
	 * @param aSamplingRate The amplitudes per second to generate.
	 * 
	 * @return The {@link MonoAmplitude} generated from the given input values.
	 */
	static MonoAmplitudeBuilder asMonoSample( double aIndex, Function<Double, Double> aFunction, double aFrequencyHz, double aAmplitude, int aXOffset, double aYOffset, int aSamplingRate ) {
		final double theTime = aIndex / ( (double) aSamplingRate );
		double theSample = aFunction.apply( 2 * Math.PI * ( aIndex - aXOffset ) * aFrequencyHz / aSamplingRate );
		theSample *= aAmplitude;
		theSample += aYOffset;
		return new MonoAmplitudeBuilderImpl( theTime, theSample );
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	static CurveFunctionSoundAmplitudeBuilder build() {
		return new CurveFunctionSoundAmplitudeBuilderImpl();
	}
}
