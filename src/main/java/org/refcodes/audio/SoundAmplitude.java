// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import org.refcodes.mixin.IndexAccessor;
import org.refcodes.mixin.TimeStampAccessor;

/**
 * A {@link SoundAmplitude} represents a sample assigned to an according time
 * positioning. A sample is a value or set of values at a point in time. In case
 * of mono audio, a sample represents a single value, in case of stereo audio, a
 * sample represents a set of values, one value represents one channel (e.g. two
 * channels when we have a left and a right speaker).
 */
public interface SoundAmplitude extends SamplingRateAccessor, IndexAccessor, TimeStampAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the sample's data for the according time positioning. A sample is
	 * a value or set of values at a point in time. In case of mono audio, a
	 * sample represents a single value, in case of stereo audio, a sample
	 * represents a set of values, one value represents one channel (e.g. two
	 * channels when we have a left and a right speaker).
	 * 
	 * @return The amplitude's data for the according time positioning.
	 */
	double[] getSampleData();

	/**
	 * Determines the number of channels this sample is ought for.
	 * 
	 * @return The number of channels this sample feeds.
	 */
	default int getChannelCount() {
		return getSampleData().length;
	}

	/**
	 * Returns the position in time of this sound sample in ms.
	 * 
	 * @return The sound sample's time positioning.
	 */
	@Override
	double getTimeStamp();

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
