// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * Base definition of a WAV sample writer.
 * 
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeWriter} implementation is to operate on.
 * @param <B> The {@link CsvAmplitudeWriter} implementing this
 *        {@link CsvAmplitudeWriter}.
 */
public interface CsvAmplitudeWriter<S extends SoundAmplitude, B extends CsvAmplitudeWriter<S, B>> extends AmplitudeWriter<S, B> {

}
