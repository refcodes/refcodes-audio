// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * The {@link AbstractLineOutAmplitudeWriter} provides a foundation means to
 * write sound samples to a line-out device.
 * 
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeWriter} implementation is to operate on.
 * @param <B> The {@link AmplitudeWriter} implementing this
 *        {@link AbstractLineOutAmplitudeWriter}.
 */
public abstract class AbstractLineOutAmplitudeWriter<S extends SoundAmplitude, B extends LineOutAmplitudeWriter<S, B>> implements LineOutAmplitudeWriter<S, B> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	protected static final long MAX_16_BIT = 65535;
	protected static final long MAX_8_BIT = 255;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected BitsPerAmplitude _bitsPerSample = BitsPerAmplitude.HIGH_RES;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractLineOutAmplitudeWriter}.
	 */
	public AbstractLineOutAmplitudeWriter() {}

	/**
	 * Constructs an {@link AbstractLineOutAmplitudeWriter} with the given
	 * {@link BitsPerAmplitude} to use.
	 * 
	 * @param aBitsPerAmplitude The bits/amplitude to use when doing audio
	 *        playback.
	 */
	public AbstractLineOutAmplitudeWriter( BitsPerAmplitude aBitsPerAmplitude ) {
		_bitsPerSample = aBitsPerAmplitude;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBitsPerAmplitude( BitsPerAmplitude aBitsPerAmplitude ) {
		_bitsPerSample = aBitsPerAmplitude;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BitsPerAmplitude getBitsPerAmplitude() {
		return _bitsPerSample;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected long toWavSample( double eSampleData ) {
		return switch ( _bitsPerSample ) {
		case HIGH_RES -> (long) ( eSampleData * ( MAX_16_BIT / 2 ) ); // PCM SIGNED
		case LOW_RES -> (long) ( eSampleData * ( MAX_8_BIT / 2 ) ); // PCM SIGNED
		};
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Produces a line-out {@link SourceDataLine} instance for writing samples
	 * to.
	 * 
	 * @param aSoundAmplitude The {@link SoundAmplitude} from which to get the
	 *        according metrics.
	 * @param aBitsPerAmplitude The preferred bits/amplitude.
	 * 
	 * @return An instance of the {@link SourceDataLine} to which to write data.
	 * 
	 * @throws LineUnavailableException thrown in case the audio-line cannot be
	 *         acquired.
	 */
	protected static SourceDataLine toLineOut( SoundAmplitude aSoundAmplitude, BitsPerAmplitude aBitsPerAmplitude ) throws LineUnavailableException {
		AudioSystem.getMixerInfo(); // Init the syound system...?!?
		// @formatter:off
		final AudioFormat format = new AudioFormat(
			AudioFormat.Encoding.PCM_SIGNED, // Encoding
			aSoundAmplitude.getSamplingRate(), // Sample Rate
			aBitsPerAmplitude.getBitCount() * aSoundAmplitude.getChannelCount(), // Sample size in Bits 
			aSoundAmplitude.getChannelCount(),  // Channels
			aBitsPerAmplitude.getByteCount() * aSoundAmplitude.getChannelCount(), //  Number of bytes in each frame
			aSoundAmplitude.getSamplingRate(), // Number of frames per second
			true // True = big endian, false = little endian
		);
		// @formatter:on

		final DataLine.Info info = new DataLine.Info( SourceDataLine.class, format );
		final SourceDataLine theLine = (SourceDataLine) AudioSystem.getLine( info );
		theLine.open();
		theLine.start();
		return theLine;
	}
}
