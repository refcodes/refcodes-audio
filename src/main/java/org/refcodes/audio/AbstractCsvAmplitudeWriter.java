// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.refcodes.tabular.CsvStringRecordWriter;

/**
 * The {@link AbstractCsvAmplitudeWriter} provides a foundation to write sound
 * samples to a CSV file.
 *
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeWriter} implementation is to operate on.
 * @param <B> The {@link CsvAmplitudeWriter} implementing this
 *        {@link AbstractCsvAmplitudeWriter}.
 */
public abstract class AbstractCsvAmplitudeWriter<S extends SoundAmplitude, B extends CsvAmplitudeWriter<S, B>> implements CsvAmplitudeWriter<S, B> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	static final String CHANNEL_INDEX_SEPARATOR = "_";
	static final String HEADER_SAMPLING_RATE = "sampling_rate";
	static final String HEADER_CHANNEL = "channel";
	static final String HEADER_SAMPLE_DATA = "sample_data";
	static final String HEADER_INDEX = "index";
	static final String HEADER_TIME_STAMP = "time_stamp";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CsvStringRecordWriter _csvWriter;
	protected CsvDeltaMode _deltaMode;
	protected boolean _isSamplingRateDirty = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public AbstractCsvAmplitudeWriter( File aFile ) throws FileNotFoundException {
		this( aFile, CsvDeltaMode.NONE );

	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the CSV
	 *        records to.
	 */
	public AbstractCsvAmplitudeWriter( OutputStream aOutputStream ) {
		this( aOutputStream, CsvDeltaMode.NONE );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the CSV
	 *        records to.
	 */
	public AbstractCsvAmplitudeWriter( PrintStream aPrintStream ) {
		this( aPrintStream, CsvDeltaMode.NONE );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public AbstractCsvAmplitudeWriter( File aFile, CsvDeltaMode aDeltaMode ) throws FileNotFoundException {
		this( new CsvStringRecordWriter( aFile ), aDeltaMode );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write the CSV
	 *        records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 */
	public AbstractCsvAmplitudeWriter( OutputStream aOutputStream, CsvDeltaMode aDeltaMode ) {
		this( new CsvStringRecordWriter( aOutputStream ), aDeltaMode );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aPrintStream The {@link PrintStream} where to write the CSV
	 *        records to.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 */
	public AbstractCsvAmplitudeWriter( PrintStream aPrintStream, CsvDeltaMode aDeltaMode ) {
		this( new CsvStringRecordWriter( aPrintStream ), aDeltaMode );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeWriter} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aCsvFriter The {@link CsvStringRecordWriter} with which to write
	 *        the CSV records with.
	 * @param aDeltaMode The {@link CsvDeltaMode} to use when writing the CSV
	 *        rows.
	 */
	protected AbstractCsvAmplitudeWriter( CsvStringRecordWriter aCsvFriter, CsvDeltaMode aDeltaMode ) {
		_csvWriter = aCsvFriter;
		_deltaMode = aDeltaMode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_csvWriter.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a double to a string by hackishly removing trailing zeros if
	 * there is no additional value from them unifying the decimal point.
	 * 
	 * @param aDouble The double to be converted to a string.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static String toString( double aDouble ) {
		return format( Double.toString( aDouble ) );
	}

	/**
	 * Converts a string to a double by hackishly removing trailing zeros if
	 * there is no additional value from them and unifying the decimal point.
	 * 
	 * @param aDouble The double from which to remove trailing zeros.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static double toDouble( String aDouble ) {
		return Double.valueOf( format( aDouble ) );
	}

	/**
	 * Hackishly removes trailing zeros if there is no additional value from
	 * them unifying the decimal point.
	 * 
	 * @param aDouble The double from which to remove trailing zeros.
	 * 
	 * @return The formatted double without any trailing zeros.
	 */
	protected static String format( String aDouble ) {
		int index = aDouble.indexOf( '.' );
		if ( index == -1 ) {
			index = aDouble.indexOf( ',' );
		}
		if ( index != -1 ) {
			while ( aDouble.endsWith( "0" ) ) {
				aDouble = aDouble.substring( 0, aDouble.length() - 1 );
			}
			if ( aDouble.endsWith( "." ) || aDouble.endsWith( "," ) ) {
				aDouble = aDouble.substring( 0, aDouble.length() - 1 );
			}
		}
		return aDouble;
	}
}
