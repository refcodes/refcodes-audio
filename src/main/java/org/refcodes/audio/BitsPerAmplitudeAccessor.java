// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * Provides an accessor for a bits/amplitude per channel property.
 */
public interface BitsPerAmplitudeAccessor {

	/**
	 * Retrieves the bits/amplitude per channel from the bits/amplitude per channel
	 * property.
	 * 
	 * @return The bits/amplitude per channel stored by the bits/amplitude per channel
	 *         property.
	 */
	BitsPerAmplitude getBitsPerAmplitude();

	/**
	 * Provides a mutator for a bits/amplitude per channel property.
	 */
	public interface BitsPerAmplitudeMutator {

		/**
		 * Sets the bits/amplitude per channel for the bits/amplitude per channel
		 * property.
		 * 
		 * @param aBitsPerAmplitude The bits/amplitude per channel to be stored by the
		 *        sampling rate property.
		 */
		void setBitsPerAmplitude( BitsPerAmplitude aBitsPerAmplitude );
	}

	/**
	 * Provides a builder method for a bits/amplitude per channel property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BitsPerAmplitudeBuilder<B extends BitsPerAmplitudeBuilder<B>> {

		/**
		 * Sets the bits/amplitude per channel for the bits/amplitude per channel
		 * property.
		 * 
		 * @param aBitsPerAmplitude The bits/amplitude per channel to be stored by the
		 *        sampling rate property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBitsPerAmplitude( BitsPerAmplitude aBitsPerAmplitude );
	}

	/**
	 * Provides a bits/amplitude per channel property.
	 */
	public interface BitsPerAmplitudeProperty extends BitsPerAmplitudeAccessor, BitsPerAmplitudeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link BitsPerAmplitude}
		 * (setter) as of {@link #setBitsPerAmplitude(BitsPerAmplitude)} and returns
		 * the very same value (getter).
		 * 
		 * @param aBitsPerAmplitude The {@link BitsPerAmplitude} to set (via
		 *        {@link #setBitsPerAmplitude(BitsPerAmplitude)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default BitsPerAmplitude letBitsPerAmplitude( BitsPerAmplitude aBitsPerAmplitude ) {
			setBitsPerAmplitude( aBitsPerAmplitude );
			return aBitsPerAmplitude;
		}
	}
}
