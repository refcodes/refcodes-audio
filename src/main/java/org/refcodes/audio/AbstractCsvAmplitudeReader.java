// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipException;

import org.refcodes.tabular.CsvStringRecordReader;

/**
 * The {@link AbstractCsvAmplitudeReader} provides a foundation to read sound
 * samples from a CSV file.
 * 
 * @param <S> The {@link SoundAmplitude} (sub-)type on which the
 *        {@link AmplitudeWriter} implementation is to operate on.
 */
public abstract class AbstractCsvAmplitudeReader<S extends SoundAmplitude> implements AmplitudeReader<S> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CsvStringRecordReader _csvReader;
	private boolean _hasHeader = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractCsvAmplitudeReader} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aFile The {@link File} where to write the CSV records to.
	 * 
	 * @throws IOException thrown in case there was an I/O related problem.
	 * @throws ZipException Signals that a Zip exception of some sort has
	 *         occurred.
	 */
	public AbstractCsvAmplitudeReader( File aFile ) throws IOException {
		this( new CsvStringRecordReader( aFile ) );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeReader} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aInputStream The {@link InputStream} where to write the CSV
	 *        records to.
	 *
	 * @throws IOException thrown in case there was an I/O related problem.
	 */
	public AbstractCsvAmplitudeReader( InputStream aInputStream ) throws IOException {
		this( new CsvStringRecordReader( aInputStream ) );
	}

	/**
	 * Constructs the {@link AbstractCsvAmplitudeReader} for writing sound
	 * samples to a CSV file or stream.
	 * 
	 * @param aCsvReader The {@link CsvStringRecordReader} with which to write
	 *        the CSV records with.
	 */
	protected AbstractCsvAmplitudeReader( CsvStringRecordReader aCsvReader ) {
		_csvReader = aCsvReader;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		try {
			doProbeHeader();
		}
		catch ( IOException ignore ) { /* ignore */ }
		return _csvReader.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_csvReader.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Reads the header if it has not been read yet.
	 * 
	 * @throws IOException thrown in case of an IO related problem.
	 */
	protected void doProbeHeader() throws IOException {
		if ( !_hasHeader ) {
			synchronized ( this ) {
				if ( !_hasHeader ) {
					_csvReader.readHeader();
					_hasHeader = true;
				}
			}
		}
	}
}
