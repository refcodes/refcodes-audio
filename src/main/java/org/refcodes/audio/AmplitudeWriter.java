// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.IOException;

import org.refcodes.audio.SamplingRateAccessor.SamplingRateBuilder;
import org.refcodes.audio.SamplingRateAccessor.SamplingRateProperty;
import org.refcodes.io.RowWriter;

/**
 * The {@link AmplitudeWriter} writes sound samples to a stream or a file.
 * 
 * @param <S> The type of the sample to be used.
 * @param <B> The {@link AmplitudeWriter} implementing this {@link AmplitudeWriter}.
 */
public interface AmplitudeWriter<S extends SoundAmplitude, B extends AmplitudeWriter<S, B>> extends AutoCloseable, RowWriter<S>, SamplingRateBuilder<B>, SamplingRateProperty {

	/**
	 * Writes the next samples, one sample for each channel. Provide one sample
	 * for mono audio, two samples for stereo audio and so on. In case you
	 * provide more or less samples than channels being supported by the writer,
	 * then it is up to the writer whether to duplicate the samples or calculate
	 * an average or the like.
	 *
	 * @param aAmplitude The amplitudes, one for each channel.
	 */
	// void writeNextRaw( T aAmplitude );

	/**
	 * Writes the next samples, one sample for each channel. Provide one sample
	 * for mono audio, two samples for stereo audio and so on. In case you
	 * provide more or less samples than channels being supported by the writer,
	 * then it is up to the writer whether to duplicate the samples or calculate
	 * an average or the like.
	 *
	 * @param aAmplitude The amplitudes, one for each channel.
	 * 
	 * @throws IOException thrown in case writing the sample caused an I/O
	 *         related problem.
	 */
	@Override
	void writeNext( S aAmplitude ) throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	void close() throws IOException;

}
