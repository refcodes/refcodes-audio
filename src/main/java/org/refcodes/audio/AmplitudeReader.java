// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.IOException;

import org.refcodes.io.RowReader;
import org.refcodes.mixin.IndexAccessor;

/**
 * The {@link AmplitudeReader} reads sound samples from a stream or a file.
 * 
 * @param <S> The type of the sample to be used.
 */
public interface AmplitudeReader<S extends SoundAmplitude> extends AutoCloseable, RowReader<S>, SamplingRateAccessor, IndexAccessor {

	/**
	 * Reads the next {@link SoundAmplitude}, equivalent to {@link #nextRow()} with
	 * a semantically stronger method name..
	 *
	 * @return The accordingly read {@link SoundAmplitude}.
	 * 
	 * @throws IOException thrown in case there was an I/O related problem.
	 */
	default S nextSample() throws IOException {
		return nextRow();
	}
}
