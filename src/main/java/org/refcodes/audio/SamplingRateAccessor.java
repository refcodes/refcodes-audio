// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

/**
 * Provides an accessor for a sampling rate property.
 */
public interface SamplingRateAccessor {

	/**
	 * Retrieves the sampling rate from the sampling rate property.
	 * 
	 * @return The sampling rate stored by the sampling rate property.
	 */
	int getSamplingRate();

	/**
	 * Provides a mutator for a sampling rate property.
	 */
	public interface SamplingRateMutator {

		/**
		 * Sets the sampling rate for the sampling rate property.
		 * 
		 * @param aSamplingRate The sampling rate to be stored by the sampling
		 *        rate property.
		 */
		void setSamplingRate( int aSamplingRate );
	}

	/**
	 * Provides a builder method for a sampling rate property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SamplingRateBuilder<B extends SamplingRateBuilder<B>> {

		/**
		 * Sets the sampling rate for the sampling rate property.
		 * 
		 * @param aSamplingRate The sampling rate to be stored by the sampling
		 *        rate property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSamplingRate( int aSamplingRate );
	}

	/**
	 * Provides a sampling rate property.
	 */
	public interface SamplingRateProperty extends SamplingRateAccessor, SamplingRateMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setSamplingRate(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aSamplingRate The integer to set (via
		 *        {@link #setSamplingRate(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letSamplingRate( int aSamplingRate ) {
			setSamplingRate( aSamplingRate );
			return aSamplingRate;
		}
	}
}
