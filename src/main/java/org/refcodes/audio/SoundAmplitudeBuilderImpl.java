// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.util.Arrays;

/**
 * A {@link SoundAmplitudeBuilder} represents multiple amplitudes (one for each
 * channel,e.g. two in case we have classical stereo sound) assigned to an
 * according time positioning.
 */
public class SoundAmplitudeBuilderImpl implements SoundAmplitudeBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private double _timeStamp = -1;
	private int _sampligRate = -1;
	private double[] _sampleData = null;
	private long _index = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link SoundAmplitudeImpl} instance with the according sound
	 * sample's settings
	 * 
	 * @param aSoundAmplitude The sound sample from which to create a new instance.
	 */
	public SoundAmplitudeBuilderImpl( SoundAmplitude aSoundAmplitude ) {
		_index = aSoundAmplitude.getIndex();
		_timeStamp = aSoundAmplitude.getTimeStamp();
		_sampligRate = aSoundAmplitude.getSamplingRate();
		if ( aSoundAmplitude.getSampleData() != null ) {
			_sampleData = Arrays.copyOf( aSoundAmplitude.getSampleData(), aSoundAmplitude.getChannelCount() );
		}
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public SoundAmplitudeBuilderImpl( double[] aAmplitudeData ) {
		_sampleData = aAmplitudeData;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public SoundAmplitudeBuilderImpl( double aTimeMillis, double[] aAmplitudeData ) {
		_timeStamp = aTimeMillis;
		_sampleData = aAmplitudeData;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public SoundAmplitudeBuilderImpl( double aTimeMillis, double[] aAmplitudeData, int aSamplingRate ) {
		_timeStamp = aTimeMillis;
		_sampleData = aAmplitudeData;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public SoundAmplitudeBuilderImpl( double[] aAmplitudeData, int aSamplingRate ) {
		_sampleData = aAmplitudeData;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public SoundAmplitudeBuilderImpl( long aIndex, double[] aAmplitudeData ) {
		_index = aIndex;
		_sampleData = aAmplitudeData;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public SoundAmplitudeBuilderImpl( long aIndex, double[] aAmplitudeData, int aSamplingRate ) {
		_index = aIndex;
		_sampleData = aAmplitudeData;
		_sampligRate = aSamplingRate;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 */
	public SoundAmplitudeBuilderImpl( long aIndex, double aTimeMillis, double[] aAmplitudeData ) {
		_index = aIndex;
		_timeStamp = aTimeMillis;
		_sampleData = aAmplitudeData;
	}

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aTimeMillis The time positioning of this sound sample.
	 * @param aAmplitudeData The amplitude (set of values representing the channels,
	 *        one channel per value) for the related time positioning.
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public SoundAmplitudeBuilderImpl( long aIndex, double aTimeMillis, double[] aAmplitudeData, int aSamplingRate ) {
		_index = aIndex;
		_timeStamp = aTimeMillis;
		_sampleData = aAmplitudeData;
		_sampligRate = aSamplingRate;
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link SoundAmplitudeBuilder} instance with the according time
	 * and sample settings.
	 * 
	 * @param aIndex The index of the sample according to its position in the
	 *        sample sequence (e.g. sound file).
	 * @param aSamplingRate The sampling rate for the given sample.
	 */
	public SoundAmplitudeBuilderImpl( long aIndex, int aSamplingRate ) {
		_index = aIndex;
		_sampligRate = aSamplingRate;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double[] getSampleData() {
		return _sampleData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTimeStamp() {
		return _timeStamp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSamplingRate() {
		return _sampligRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getIndex() {
		return _index;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIndex( long aIndex ) {
		_index = aIndex;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void increaseIndex() {
		_index++;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decreaseIndex() {
		_index--;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSampleData( double[] aAmplitudeData ) {
		_sampleData = aAmplitudeData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTimeStamp( double aTimeMillis ) {
		_timeStamp = aTimeMillis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSamplingRate( int aSamplingRate ) {
		_sampligRate = aSamplingRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "SoundAmplitudeBuilderImpl [index=" + getIndex() + ", timeStamp=" + getTimeStamp() + ", sampleData=" + Arrays.toString( getSampleData() ) + ", sampligRate=" + getSamplingRate() + "]";
	}
}
