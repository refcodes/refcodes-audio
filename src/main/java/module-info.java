module org.refcodes.audio {
	requires transitive org.refcodes.exception;
	requires org.refcodes.mixin;
	requires org.refcodes.io;
	requires org.refcodes.tabular;
	requires org.refcodes.numerical;
	requires org.refcodes.runtime;

	exports org.refcodes.audio;
}
