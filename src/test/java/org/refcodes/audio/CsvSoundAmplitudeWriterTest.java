// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * Tests the {@link CsvSoundAmplitudeWriter} type.
 */
public class CsvSoundAmplitudeWriterTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	static String DELTA_ALL = "index;time_stamp;sample_data;sampling_rate\n" + "0;0;0;44100\n" + "1;2.2675736961451248E-5;;\n" + "2;4.5351473922902495E-5;1;\n" + "3;6.802721088435374E-5;;\n" + "4;9.070294784580499E-5;2;\n" + "5;1.1337868480725624E-4;;\n" + "6;1.3605442176870748E-4;3;\n" + "7;1.5873015873015873E-4;;\n" + "8;1.8140589569160998E-4;4;\n" + "9;2.0408163265306123E-4;;\n" + "10;0.1;5;100\n" + "11;0.11;6;\n" + "12;0.12;;\n" + "13;0.13;7;\n" + "14;0.14;;\n" + "15;0.15;8;\n" + "16;0.16;;\n" + "17;0.17;9;\n" + "18;0.18;;\n";
	static String DELTA_NONE = "index;time_stamp;sample_data;sampling_rate\n" + "0;0;0;44100\n" + "1;2.2675736961451248E-5;0;44100\n" + "2;4.5351473922902495E-5;1;44100\n" + "3;6.802721088435374E-5;1;44100\n" + "4;9.070294784580499E-5;2;44100\n" + "5;1.1337868480725624E-4;2;44100\n" + "6;1.3605442176870748E-4;3;44100\n" + "7;1.5873015873015873E-4;3;44100\n" + "8;1.8140589569160998E-4;4;44100\n" + "9;2.0408163265306123E-4;4;44100\n" + "10;0.1;5;100\n" + "11;0.11;6;100\n" + "12;0.12;6;100\n" + "13;0.13;7;100\n" + "14;0.14;7;100\n" + "15;0.15;8;100\n" + "16;0.16;8;100\n" + "17;0.17;9;100\n" + "18;0.18;9;100\n";
	static String DELTA_SAMPLING_RATE = "index;time_stamp;sample_data;sampling_rate\n" + "0;0;0;44100\n" + "1;2.2675736961451248E-5;0;\n" + "2;4.5351473922902495E-5;1;\n" + "3;6.802721088435374E-5;1;\n" + "4;9.070294784580499E-5;2;\n" + "5;1.1337868480725624E-4;2;\n" + "6;1.3605442176870748E-4;3;\n" + "7;1.5873015873015873E-4;3;\n" + "8;1.8140589569160998E-4;4;\n" + "9;2.0408163265306123E-4;4;\n" + "10;0.1;5;100\n" + "11;0.11;6;\n" + "12;0.12;6;\n" + "13;0.13;7;\n" + "14;0.14;7;\n" + "15;0.15;8;\n" + "16;0.16;8;\n" + "17;0.17;9;\n" + "18;0.18;9;\n";
	static String DELTA_SAMPLE_DATA = "index;time_stamp;sample_data;sampling_rate\n" + "0;0;0;44100\n" + "1;2.2675736961451248E-5;;44100\n" + "2;4.5351473922902495E-5;1;44100\n" + "3;6.802721088435374E-5;;44100\n" + "4;9.070294784580499E-5;2;44100\n" + "5;1.1337868480725624E-4;;44100\n" + "6;1.3605442176870748E-4;3;44100\n" + "7;1.5873015873015873E-4;;44100\n" + "8;1.8140589569160998E-4;4;44100\n" + "9;2.0408163265306123E-4;;44100\n" + "10;0.1;5;100\n" + "11;0.11;6;100\n" + "12;0.12;;100\n" + "13;0.13;7;100\n" + "14;0.14;;100\n" + "15;0.15;8;100\n" + "16;0.16;;100\n" + "17;0.17;9;100\n" + "18;0.18;;100\n";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvSoundSampleWriterDeltaNone() throws IOException {
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		final CsvDeltaMode theSamplingRate = CsvDeltaMode.NONE;
		try ( CsvSoundAmplitudeWriter theWriter = new CsvSoundAmplitudeWriter( theOut, theSamplingRate ); CsvSoundAmplitudeWriter theVerifyWriter = new CsvSoundAmplitudeWriter( System.out, theSamplingRate ) ) {
			int j;
			for ( int i = 0; i < 10; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			theWriter.setSamplingRate( 100 );
			theVerifyWriter.setSamplingRate( 100 );
			for ( int i = 11; i < 20; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			final String theCsv = theOut.toString().replaceAll( "\\r", "" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "---" );
				System.out.println( theCsv );
			}
			assertEquals( DELTA_NONE, theCsv );
		}
	}

	@Test
	public void testCsvSoundSampleWriterDeltaSamplingRate() throws IOException {
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		final CsvDeltaMode theSamplingRate = CsvDeltaMode.SAMPLING_RATE;
		try ( CsvSoundAmplitudeWriter theWriter = new CsvSoundAmplitudeWriter( theOut, theSamplingRate ); CsvSoundAmplitudeWriter theVerifyWriter = new CsvSoundAmplitudeWriter( System.out, theSamplingRate ) ) {
			int j;
			for ( int i = 0; i < 10; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			theWriter.setSamplingRate( 100 );
			theVerifyWriter.setSamplingRate( 100 );
			for ( int i = 11; i < 20; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			final String theCsv = theOut.toString().replaceAll( "\\r", "" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "---" );
				System.out.println( theCsv );
			}
			assertEquals( DELTA_SAMPLING_RATE, theCsv );
		}
	}

	@Test
	public void testCsvSoundSampleWriterDeltaAmplitudeData() throws IOException {
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		final CsvDeltaMode theSamplingRate = CsvDeltaMode.SAMPLE_DATA;
		try ( CsvSoundAmplitudeWriter theWriter = new CsvSoundAmplitudeWriter( theOut, theSamplingRate ); CsvSoundAmplitudeWriter theVerifyWriter = new CsvSoundAmplitudeWriter( System.out, theSamplingRate ) ) {
			int j;
			for ( int i = 0; i < 10; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			theWriter.setSamplingRate( 100 );
			theVerifyWriter.setSamplingRate( 100 );
			for ( int i = 11; i < 20; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			final String theCsv = theOut.toString().replaceAll( "\\r", "" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "---" );
				System.out.println( theCsv );
			}
			assertEquals( DELTA_SAMPLE_DATA, theCsv );
		}
	}

	@Test
	public void testCsvSoundSampleWriterDeltaAll() throws IOException {
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		final CsvDeltaMode theSamplingRate = CsvDeltaMode.ALL;
		try ( CsvSoundAmplitudeWriter theWriter = new CsvSoundAmplitudeWriter( theOut, theSamplingRate ); CsvSoundAmplitudeWriter theVerifyWriter = new CsvSoundAmplitudeWriter( System.out, theSamplingRate ) ) {
			int j;
			for ( int i = 0; i < 10; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			theWriter.setSamplingRate( 100 );
			theVerifyWriter.setSamplingRate( 100 );
			for ( int i = 11; i < 20; i++ ) {
				j = i / 2;
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					theVerifyWriter.writeNext( j );
				}
				theWriter.writeNext( j );
			}
			final String theCsv = theOut.toString().replaceAll( "\\r", "" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "---" );
				System.out.println( theCsv );
			}
			assertEquals( DELTA_ALL, theCsv );
		}
	}
}
