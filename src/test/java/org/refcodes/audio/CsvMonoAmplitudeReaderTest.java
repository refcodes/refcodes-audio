// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class CsvMonoAmplitudeReaderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvMonoSampleReaderDeltaNone() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( CsvSoundAmplitudeWriterTest.DELTA_NONE.getBytes() );
		try ( CsvMonoAmplitudeReader theReader = new CsvMonoAmplitudeReader( theInputStream ) ) {
			SoundAmplitude eSample;
			while ( theReader.hasNext() ) {
				eSample = theReader.nextRow();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eSample.toString() );
				}
			}
		}
	}

	@Test
	public void testCsvMonoSampleReaderDeltaAmplitudeData() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( CsvSoundAmplitudeWriterTest.DELTA_SAMPLE_DATA.getBytes() );
		try ( CsvMonoAmplitudeReader theReader = new CsvMonoAmplitudeReader( theInputStream ) ) {
			SoundAmplitude eSample;
			while ( theReader.hasNext() ) {
				eSample = theReader.nextRow();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eSample.toString() );
				}
			}
		}
	}

	@Test
	public void testCsvMonoSampleReaderDeltaSamplingRate() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( CsvSoundAmplitudeWriterTest.DELTA_SAMPLING_RATE.getBytes() );
		try ( CsvMonoAmplitudeReader theReader = new CsvMonoAmplitudeReader( theInputStream ) ) {
			SoundAmplitude eSample;
			while ( theReader.hasNext() ) {
				eSample = theReader.nextRow();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eSample.toString() );
				}
			}
		}
	}

	@Test
	public void testCsvMonoSampleReaderDeltaAll() throws IOException {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( CsvSoundAmplitudeWriterTest.DELTA_ALL.getBytes() );
		try ( CsvMonoAmplitudeReader theReader = new CsvMonoAmplitudeReader( theInputStream ) ) {
			SoundAmplitude eSample;
			while ( theReader.hasNext() ) {
				eSample = theReader.nextRow();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eSample.toString() );
				}
			}
		}
	}
}
