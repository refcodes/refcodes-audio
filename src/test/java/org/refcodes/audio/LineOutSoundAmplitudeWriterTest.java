// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.audio;

import java.io.IOException;
import java.util.function.Function;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Mixer.Info;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class LineOutSoundAmplitudeWriterTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled("Enable to hear an 880Hz sound when the test is fine!")
	public void testWriteSamples() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			toMixerInfo();
		}
		final int LENGTH_IN_SECONDS = 1;
		final int SAMPLING_RATE = SamplingRate.AUDIO_CD.getSamplesPerSecond();
		final int SAMPLE_COUNT = ( LENGTH_IN_SECONDS * SAMPLING_RATE ) + 1;
		final double FREQUENCY_IN_HZ = 880;
		final double AMPLITUDE = 1;
		final int X_OFFSET = 0;
		final double Y_OFFSET = 0;
		final CurveFunctionFunction TRIGONOMETRIC_FUNCTION = CurveFunctionFunction.SINE;
		try ( LineOutSoundAmplitudeWriter theLineOutWriter = new LineOutSoundAmplitudeWriter() ) {
			theLineOutWriter.setSamplingRate( SAMPLING_RATE );
			theLineOutWriter.setBitsPerAmplitude( BitsPerAmplitude.LOW_RES );
			MonoAmplitudeBuilder eSample;
			for ( int i = 0; i < SAMPLE_COUNT; i++ ) {
				eSample = toSample( i, TRIGONOMETRIC_FUNCTION.getFunction(), FREQUENCY_IN_HZ, AMPLITUDE, X_OFFSET, Y_OFFSET, SAMPLING_RATE );
				theLineOutWriter.writeNext( eSample.getMonoData() );
			}
		}
		try {
			Thread.sleep( LENGTH_IN_SECONDS * 1000 );
		}
		catch ( InterruptedException e ) {}
	}

	private void toMixerInfo() {
		final Info[] theInfos = AudioSystem.getMixerInfo();
		for ( Mixer.Info theInfo : theInfos ) {
			System.out.println( theInfo.getName() );
			System.out.println( theInfo.getVendor() );
			System.out.println( theInfo.getVersion() );
			System.out.println( theInfo.getDescription() );
			System.out.println();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static MonoAmplitudeBuilder toSample( long aIndex, Function<Double, Double> aTrigonemetricFunction, double aFrequencyHz, double aAmplitude, int aXOffset, double aYOffset, int aSamplingRate ) {
		final double theTime = ( (double) aIndex ) / ( (double) aSamplingRate );
		double theSample = aTrigonemetricFunction.apply( 2 * Math.PI * ( aIndex - aXOffset ) * aFrequencyHz / aSamplingRate );
		theSample *= aAmplitude;
		theSample += aYOffset;
		return new MonoAmplitudeBuilderImpl( aIndex, theTime, theSample, aSamplingRate );
	}
}
